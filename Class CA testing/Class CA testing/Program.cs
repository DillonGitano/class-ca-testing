﻿using Class_CA_testing.Classes;
using System;
namespace Class_CA_testing
{
    class Program
    {
        static void Main(string[] args)
        {
            //static class and static method
            var myFirstInt = CAclass1.GetMeSomeInt();

            //regular class and method
            var msc = new CAclass2();

            var mySecondInt = msc.GetMeSomeInt();

            var result = MyNewMethod(64);

            var result2 = MyNewMethod((decimal)17);

            Console.WriteLine();

        }

        /*private method only accessible inside the Program class
    static, because the calling method is also static
    int is the return type, meaning I'm going to get an integer back
    MyNewMethod is the method name
    param is an integer parameter
    this divides param value by 32 and returns the result*/

        private static int MyNewMethod(int param)
        {
            return param / 32;
        }

        private static decimal MyNewMethod(decimal param)
        {
            return param / (decimal)32;
        }
    }
}
